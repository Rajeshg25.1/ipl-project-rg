import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Problem4 {
    public static void main(String[] args) {

        // Q.4: For the year 2015 get the top economical bowlers.

//  Go for the deliveries dataset

// top economical bowlers = total runs conceded by the bowler/ total number of overs bowled by the bowler

    try {
        String path="/home/rajesh/Desktop/Practice/IPL_Project/src/matches.csv";

        // This is for matches file reading

        File dataset = new File(path);

        FileReader fileReader = new FileReader(dataset);

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        HashSet<String> match_id = new HashSet<>();

        String line; // storing each line of matches dataset

        while ((line = bufferedReader.readLine()) != null) {

            String str[]=line.split(","); /// make an array of strings. and store the value and give index before , and make a single string

            // storing the id's of all 2015 matches
            if (str[1].equals("2015"))
            {

                match_id.add(str[0]);
            }

        }

//            System.out.println(match_id);

        bufferedReader.close();

        fileReader.close();


        //
        String path1 = "/home/rajesh/Desktop/Practice/IPL_Project/src/deliveries.csv";

        File delivery_dataset = new File(path1);

        FileReader read_file = new FileReader(delivery_dataset);

        BufferedReader buff_readFile = new BufferedReader(read_file);

        String each_line;

        HashMap<String, Float> bowler_total_bowls = new HashMap<>();

        HashMap<String , Float> bowlers_total_run = new HashMap<>();

        while ((each_line = buff_readFile.readLine()) != null)
        {
            String strArr[] = each_line.split(",");

//                System.out.print(strArr[1] + " " );

            /// checking the match_id
            if(match_id.contains(strArr[0])) {
                // counting the occurrences of bowlers. It gives how many bowls thrown by each bowlers
                Float wide_bowlRun = Float.parseFloat(strArr[10])  ;

                Float no_bowlRun = Float.parseFloat(strArr[13])  ;

                if (bowler_total_bowls.containsKey(strArr[8])) {

                    Float count = bowler_total_bowls.get(strArr[8]);

//                    Float temp = Float.parseFloat(strArr[17]);


                    bowler_total_bowls.put(strArr[8], count + 1.0f -wide_bowlRun - no_bowlRun);
                }
                else {
                    bowler_total_bowls.put(strArr[8], 1.0f - wide_bowlRun - no_bowlRun);
                }

            }


            // Calculating total runs given by the each bowlers



            if(match_id.contains(strArr[0]))
            {
                Float by_runs = Float.parseFloat(strArr[11]);

                Float legby_runs = Float.parseFloat(strArr[12]);

                Float penalty_runs = Float.parseFloat(strArr[14]);

                Float total_runs = Float.parseFloat(strArr[17]);

                if(bowlers_total_run.containsKey(strArr[8]))
                {
                    Float cnt = bowlers_total_run.get(strArr[8]);

                    bowlers_total_run.put(strArr[8] , cnt + total_runs - by_runs - legby_runs - penalty_runs );

                }
                else
                {
                    bowlers_total_run.put(strArr[8] , total_runs - by_runs - legby_runs - penalty_runs);
                }




            }



        }

        // For seeing total no. of bowls given by bowlers in IPL season 2015

       /* for(Map.Entry<String , Float> it : bowler_total_runs.entrySet())
        {
            System.out.println("Name of the Bowler: " + it.getKey() + ", and total bolls thrown by him : " + it.getValue());

        }*/
//            System.out.println(map);


        /// Calculating total no. of overs bowled by the bowlers
        HashMap<String, Float> bowler_total_Overs = new HashMap<>();

        // Iterating over the bowler total runs
        for(Map.Entry<String, Float> it : bowler_total_bowls.entrySet())
        {
            String name = it.getKey();

            Float total_over = it.getValue()/6.0f;

            bowler_total_Overs.put(name , total_over);

        }



        // For seeing bowlers, bowled total overs

      /*  for (Map.Entry<String ,Float> it : bowler_total_Overs.entrySet())
        {
            System.out.println("Name : " + it.getKey() + " ,Total Overs: " + it.getValue());
        }
        */
       


        // Going to calculate the economy of the each bowlers


//        For over we have : bowler_total_overs
//        For runs we have  : bowlers_total_runs

        TreeMap<Float , String> economical_bowler = new TreeMap<>();

        for(Map.Entry<String , Float> it : bowler_total_Overs.entrySet())
        {
            String name = it.getKey();

            Float over = it.getValue();

            if(bowlers_total_run.containsKey(name))
            {
                Float run = bowlers_total_run.get(name);

                Float economy = run/over;

                economical_bowler.put(economy , name);

            }

        }


        // Display the top economical bowlers in IPL 2015 season

        for (Map.Entry<Float , String> it : economical_bowler.entrySet())
        {
            System.out.println("Name of the Bowler: " + it.getValue() + " and economy of this bowlers is: " + it.getKey());
        }
//        System.out.println(economical_bowler);











    }
    catch (IOException e)
    {
        System.out.println("Error occurred by " + e);

        System.out.println("Sorry! for inconvenience.");
    }


    }
}
