import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Problem5 {
    public static void main(String[] args) {
//        Q.5 Create your own scenario. (How many runs made by each batsman in all the IPL season)


        try {

            String path1 = "/home/rajesh/Desktop/Practice/IPL_Project/src/deliveries.csv";


//            This is for deliveries file



            File delivery_dataset = new File(path1);

            FileReader read_file = new FileReader(delivery_dataset);

            BufferedReader buff_readFile = new BufferedReader(read_file);

            String each_line;

            HashMap<String, Integer> map = new HashMap<>();

            while ((each_line = buff_readFile.readLine()) != null)
            {
                String strArr[] = each_line.split(",");

//                System.out.print(strArr[1] + " " );

                    if(map.containsKey(strArr[6]))
                    {
                        Integer count = map.get(strArr[6]);

                        Integer temp = Integer.parseInt(strArr[15]);
//                       Integer temp  = Integer.valueOf(strArr[15]);
//                       System.out.println(count +  " , " + temp);
                        map.put(strArr[6] , count + temp);
                    }
                    else
                    {
                        map.put(strArr[6] , 0);
                    }






            }


            for(Map.Entry<String , Integer> it : map.entrySet())
            {
                System.out.println("Name of the Player: " + it.getKey() + ", and runs made by him in all IPL season: " + it.getValue());

            }
//            System.out.println(map);


        }

        catch (IOException e) {

            e.printStackTrace();
        }





    }
}
