import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Problem3 {
    public static void main(String[] args) {

        // Q3. For the year 2016 get the extra runs conceded per team.

        try {

            String path1 = "/home/rajesh/Desktop/Practice/IPL_Project/src/deliveries.csv";

            String path="/home/rajesh/Desktop/Practice/IPL_Project/src/matches.csv";

            // This is for matches file reading
            File dataset = new File(path);

            FileReader fileReader = new FileReader(dataset);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            HashSet<String> match_id = new HashSet<>();

            String line; // storing each line of matches dataset

            while ((line = bufferedReader.readLine()) != null) {
                String str[]=line.split(","); /// make an array of strings. and store the value and give index before , and make a single string

                    // storing the id's of all 2016 matches
                    if (str[1].equals("2016")) {
                        match_id.add(str[0]);
                    }

            }

//            System.out.println(match_id);

            bufferedReader.close();

            fileReader.close();


//            This is for deliveries file


            File delivery_dataset = new File(path1);

            FileReader read_file = new FileReader(delivery_dataset);

            BufferedReader buff_readFile = new BufferedReader(read_file);

            String each_line;

            HashMap<String, Integer> map = new HashMap<>();

            while ((each_line = buff_readFile.readLine()) != null)
            {
                String strArr[] = each_line.split(",");

//                System.out.print(strArr[1] + " " );
                if(match_id.contains(strArr[0]))
                {
                    Integer temp = Integer.parseInt(strArr[16]);

                    if(map.containsKey(strArr[3]))
                    {
                        Integer count = map.get(strArr[3]);

//                        System.out.println(count +  " , " + temp);
                        map.put(strArr[3] , count + temp);
                    }
                    else
                    {

                        map.put(strArr[3] ,temp);
                    }
                }


            }


            for(Map.Entry<String,Integer> it : map.entrySet())
            {
                System.out.println("Extra runs given by team is :" + it.getKey() + " and extra runs is : " + it.getValue());
            }

//            System.out.println(map);


        }
        catch (IOException e) {

            e.printStackTrace();
        }




    }

}
