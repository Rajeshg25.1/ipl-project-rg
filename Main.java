import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main{
    public static void main(String[] args) {
//        Q.1 : Number of matches played per year of all the years in IPL.


        try {
            String path="/home/rajesh/Desktop/Practice/IPL_Project/src/matches.csv";

            HashMap<String, Integer> map = new HashMap<String, Integer>() ;

            File dataset = new File(path);

            FileReader fileReader = new FileReader(dataset);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line; // storing each line of dataset

            while ((line = bufferedReader.readLine()) != null) {

                String a[]=line.split(","); /// make an array of strings. and store the value and give index before , and make a single string

                /* For showing content of the file

                for (String item: a
                     ) {
                    System.out.print(item + " ");

                }
                System.out.println(" ");
                System.out.println(a[1]);

                 */



                if(map.containsKey(a[1]) )
                {
                    Integer count = map.get(a[1]);

                    map.put(a[1] , count+1 );

                }
                else
                {
                        map.put(a[1], 1);

                }






            }
//            System.out.println(map);

            map.remove("season");

            for (Map.Entry<String, Integer> e : map.entrySet())

            {
                System.out.println("YEAR: " + e.getKey() + " ,Number of Matches played: " + e.getValue());

            }

            bufferedReader.close();

            fileReader.close();
        }

        catch (IOException e) {
            e.printStackTrace();
        }
    }
}