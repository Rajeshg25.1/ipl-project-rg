import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Problem2 {
    public static void main(String[] args) {
        // Q.2: Number of matches won of all teams over all the years of IPL.

        try {
            String path="/home/rajesh/Desktop/Practice/IPL_Project/src/matches.csv";

            HashMap<String, Integer> map = new HashMap<String, Integer>() ;

            File dataset = new File(path);

            FileReader fileReader = new FileReader(dataset);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line; // storing each line of dataset

            while ((line = bufferedReader.readLine()) != null) {

                String a[]=line.split(","); /// make an array of strings. and store the value and give index before , and make a single string

//                System.out.println(a[10]);  giving the list of winners in all the matches


                if(map.containsKey(a[10]))
                {
                    Integer count = map.get(a[10]);
                    map.put(a[10] ,count + 1);
                }
                else
                {
                    map.put(a[10] , 1);
                }



            }

            map.remove("winner");


//            Printing the matches won by all teams in all the seasons

            for(Map.Entry<String, Integer> it : map.entrySet())
            {
                System.out.println("Team: " + it.getKey() + ", Matches won by Team: " + it.getValue());
            }

            bufferedReader.close();

            fileReader.close();
        }

        catch (IOException e) {
            e.printStackTrace();
        }


    }
}
